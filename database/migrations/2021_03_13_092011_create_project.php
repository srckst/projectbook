<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->id('prj_id');
            $table->string('prj_name',100);
            $table->string('prj_detail',100);
            $table->unsignedBigInteger('prj_prt_id');
            $table->foreign('prj_prt_id')->references('prt_id')->on('project_type');
            $table->unsignedBigInteger('prj_prn_id');
            $table->foreign('prj_prn_id')->references('prn_id')->on('province');
            $table->timestamp('prj_start_date');
            $table->timestamp('prj_end_date');
            $table->float('prj_budget',12,2);
            $table->char('prj_is_support',1)->nullable();
            $table->float('prj_budget_support',12,2)->nullable();
            $table->char('prj_is_improvement_project',1)->nullable();
            $table->char('prj_is_innovation_project',1)->nullable();
            $table->char('prj_is_research_project',1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
