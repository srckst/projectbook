INSERT INTO `region` (`rgn_id`, `rgn_name`, `created_at`, `updated_at`) VALUES (1, 'กรุงเทพมหานครและปริมณฑล', '2021-03-13 18:57:21', NULL);
INSERT INTO `region` (`rgn_id`, `rgn_name`, `created_at`, `updated_at`) VALUES (2, 'กลาง', '2021-03-13 18:57:22', NULL);
INSERT INTO `region` (`rgn_id`, `rgn_name`, `created_at`, `updated_at`) VALUES (3, 'เหนือ', '2021-03-13 18:57:23', NULL);
INSERT INTO `region` (`rgn_id`, `rgn_name`, `created_at`, `updated_at`) VALUES (4, 'ภาคตะวันออกเฉียงเหนือ', '2021-03-13 18:57:24', NULL);
INSERT INTO `region` (`rgn_id`, `rgn_name`, `created_at`, `updated_at`) VALUES (5, 'ภาคใต้', '2021-03-13 18:57:25', NULL);
