<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Project;
use App\ProjectType;
use App\Province;
use App\Region;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $province = Province::all();
        $projecttype = ProjectType::all();
        $project = Project::leftJoin('project_type','prt_id','=','prj_prt_id')
        ->leftJoin('province','prn_id','=','prj_prn_id')
        ->leftJoin('region','rgn_id','=','prn_rgn_id')
        ->get();

        return view('index',[
            'province'          =>          $province,
            'projecttype'       =>          $projecttype,
            'project'           =>          $project,
        ]);
    }

    public function addproject(Request $request){
        $add = new Project;
        $add->prj_name = $request->input('prj_name');
        $add->prj_detail = $request->input('prj_detail');
        $add->prj_prt_id = $request->input('prj_prt_id');
        $add->prj_prn_id = $request->input('prj_prn_id');
        $add->prj_start_date = Carbon::parse($request->input('prj_start_date'))->toDateTimeString();
        $add->prj_end_date = Carbon::parse($request->input('prj_end_date'))->toDateTimeString();
        $add->prj_budget = $request->input('prj_budget');
        $add->prj_is_support = $request->input('prj_is_support');
        $add->prj_budget_support = $request->input('prj_budget_support');
        $add->prj_is_improvement_project = $request->input('prj_is_improvement_project');
        $add->prj_is_innovation_project = $request->input('prj_is_innovation_project');
        $add->prj_is_research_project = $request->input('prj_is_research_project');
        $add->save();
    }
    public function editproject(Request $request , $prjid){
        $edit = Project::where('prj_id','=',$prjid);
        $edit->update([
            'prj_name'                      =>      $request->input('prj_name'),
            'prj_detail'                    =>      $request->input('prj_detail'),
            'prj_prt_id'                    =>      $request->input('prj_prt_id'),
            'prj_prn_id'                    =>      $request->input('prj_prn_id'),
            'prj_start_date'                =>      $request->input('prj_start_date'),
            'prj_end_date'                  =>      $request->input('prj_end_date'),
            'prj_budget'                    =>      $request->input('prj_budget'),
            'prj_is_support'                =>      $request->input('prj_is_support'),
            'prj_budget_support'            =>      $request->input('prj_budget_support'),
            'prj_is_improvement_project'    =>      $request->input('prj_is_improvement_project'),
            'prj_is_innovation_project'     =>      $request->input('prj_is_innovation_project'),
            'prj_is_research_project'       =>      $request->input('prj_is_research_project'),
        ]);
    }
}
