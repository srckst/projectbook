<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ระบบจัดการโครงการ</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/ample-admin-lite/" />
    <link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/favicon.png') }}">
    <!-- Custom CSS -->
    <link href="{{ asset('plugins/bower_components/chartist/dist/chartist.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css') }}">
    <!-- Custom CSS -->
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-navbarbg="skin5">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-header" data-logobg="skin6">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <b class="logo-icon">
                        <img src="plugins/images/logo-icon.png" alt="homepage" />
                    </b>
                    <span class="logo-text">
                        <h3 style="color:black;">Project Book</h4>
                        </span>
                    </a>
                    <a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none"
                    href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">

                    <ul class="navbar-nav ms-auto d-flex align-items-center">
                        <li class=" in">
                            <form role="search" class="app-search d-none d-md-block me-3">
                                <input type="text" placeholder="Search..." class="form-control mt-0">
                                <a href="" class="active">
                                    <i class="fa fa-search"></i>
                                </a>
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar">
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <!-- User Profile-->
            <li class="sidebar-item pt-2">
                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('home') }}"
                    aria-expanded="false">
                    <i class="far fa-clock" aria-hidden="true"></i>
                    <span class="hide-menu">ระบบจัดการโครงการ</span>
                </a>
            </li>
            </li>
        </ul>

    </nav>
    <!-- End Sidebar navigation -->
</div>
        </aside>
        <div class="page-wrapper">
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">ระบบจัดการโครงการ</h4>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                            <div class="d-md-flex mb-3">
                                <h3 class="box-title mb-0">รายการโครงการ</h3>
                                <div class="col-xs-12 ms-auto">
                                    <button class="btn btn-primary btn-lg" type="button" name="button" data-toggle="modal"  data-target="#addprojectnoti"><i class="fa fa-plus"></i> เพิ่มโครงการ</button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="projectall" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0 text-center">ลำดับ</th>
                                            <th class="border-top-0 text-center">ชื่อโครงการ</th>
                                            <th class="border-top-0 text-center">ประเภท</th>
                                            <th class="border-top-0 text-center">จังหวัด</th>
                                            <th class="border-top-0 text-center">ภาค</th>
                                            <th class="border-top-0 text-center">เริ่มต้น</th>
                                            <th class="border-top-0 text-center"><i class="fa fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($project as $projects)
                                            <tr>
                                                <td class="text-center">{{ $loop->iteration }}</td>
                                                <td class="text-center">{{ $projects->prj_name }}</td>
                                                <td class="text-center">{{ $projects->prt_name }}</td>
                                                <td class="text-center">{{ $projects->prn_name }}</td>
                                                <td class="text-center">{{ $projects->rgn_name }}</td>
                                                <td class="text-center">{{ date('Y-m-d',strtotime($projects->prj_start_date)) }}</td>
                                                <td class="text-center"><a class="editproject" href="#" data-target="#editprojectnoti" data-toggle="modal" data-prjid="{{ $projects->prj_id }}" data-prjprnid="{{ $projects->prj_prn_id }}" data-prjprtid="{{ $projects->prj_prt_id }}" data-prjisresearchproject="{{ $projects->prj_is_research_project }}" data-prjisinnovationproject="{{ $projects->prj_is_innovation_project }}" data-prjisimprovementproject="{{ $projects->prj_is_improvement_project }}" data-prjbudgetsupport="{{ $projects->prj_budget_support }}" data-prjbudget="{{ $projects->prj_budget }}" data-prjdetail="{{ $projects->prj_detail }}" data-prjissupport="{{ $projects->prj_is_support }}" data-prjname="{{ $projects->prj_name }}" data-prtname="{{ $projects->prt_name }}" data-prnname="{{ $projects->prn_name }}" data-prjstartdate="{{ date('Y-m-d',strtotime($projects->prj_start_date)) }}" data-prjenddate="{{ date('Y-m-d',strtotime($projects->prj_end_date)) }}"><i style="font-size:25px;" class="fa fa-edit"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer text-center"> Project Book
            </footer>

        </div>
    </div>

    <div class="modal fade" id="addprojectnoti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel"><b>ข้อมูลโครงการ</b></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="addprojectform">
                    @csrf
                    <div class="modal-body">
                        <div class="col-xs-12 form-gr">
                            <div class="form-group row">
                                <label for="inputproject" class="col-sm-2 col-form-label"><b>ชื่อโครงการ</b></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="prj_name" id="inputproject" placeholder="ชื่อโครงการ" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputdetail" class="col-sm-2 col-form-label"><b>รายระเอียด</b></label>
                                <div class="col-sm-10">
                                    <textarea  name="prj_detail" rows="4" cols="80" class="form-control" id="inputdetail" placeholder="รายระเอียด"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputtype" class="col-sm-2 col-form-label"><b>ประเภท</b></label>
                                <div class="col-sm-10">
                                    <select id="inputtype" class="form-control" name="prj_prt_id" required>
                                        <option selected value="">เลือกประเภท</option>
                                        @foreach ($projecttype as $projecttypes)
                                            <option value="{{ $projecttypes->prt_id }}">{{ $projecttypes->prt_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputprovince" class="col-sm-2 col-form-label"><b>จังหวัด</b></label>
                                <div class="col-sm-10">
                                    <select id="inputprovince" class="form-control" name="prj_prn_id" required>
                                        <option selected >เลือกจังหวัด</option>
                                        @foreach ($province as $provinces)
                                            <option value="{{ $provinces->prn_id }}">{{ $provinces->prn_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputdatestart" class="col-sm-2 col-form-label"><b>เริ่ม</b></label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="prj_start_date" id="inputdatestart" required>
                                </div>
                                <label for="inputdateend" class="col-sm-2 col-form-label"><b>สิ้นสุด</b></label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="prj_end_date" id="inputdateend" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputbudget1" class="col-sm-2 col-form-label"><b>งบประมาณ</b></label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="prj_budget" id="inputbudget1" placeholder="งบประมาณ" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputbudget3" class="col-sm-2 col-form-label"><b>มีงบสนับสนุนอีก</b></label>
                                <div class="col-sm-1">
                                    <input type="radio" id="havemore" name="prj_is_support" value="1">
                                    <label class="col-sm-2 col-form-label" for="havemore">มี</label><br>
                                </div>
                                <div class="col-sm-1">
                                    <input type="radio" id="nomore" name="prj_is_support" value="0">
                                    <label class="col-sm-2 col-form-label" for="nomore">ไม่มี</label><br>
                                </div>
                            </div>
                            <div id="showmorebudget" class="form-group row">
                                <label for="inputbudget2" class="col-sm-2 col-form-label"><b>งบประมาณ</b></label>
                                <div class="col-sm-10">
                                    <input type="number" name="prj_budget_support" class="form-control" id="inputbudget2" placeholder="งบประมาณ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputstyle" class="col-sm-2 col-form-label"><b>ลักษณะโครงการ</b></label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="prj_is_improvement_project" id="inputstyle1" value="1">
                                    <label class="col-sm-5 col-form-label" for="inputstyle1">โครงการปรับปรุงงาน (Improvement project)</label><br>
                                    <input type="checkbox" name="prj_is_innovation_project" id="inputstyle2" value="1">
                                    <label class="col-sm-5 col-form-label" for="inputstyle2">โครงการนวัตกรรม (Innovative project)</label><br>
                                    <input type="checkbox" name="prj_is_research_project" id="inputstyle3" value="1">
                                    <label class="col-sm-7 col-form-label" for="inputstyle3">โครงการวิจัยและพัฒนา (research and development project)</label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="color:#FFFFFF;" type="submit" class="btn btn-success"><i class="fa fa-save"></i> บันทึก</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editprojectnoti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลโครงการ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="editprojectform">
                    <input id="prjhiddenid" type="hidden" value="">
                    @csrf
                    <div class="modal-body">
                        <div class="col-xs-12 form-gr">
                            <div class="form-group row">
                                <label for="editinputproject" class="col-sm-2 col-form-label"><b>ชื่อโครงการ</b></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="prj_name" id="editinputproject" placeholder="ชื่อโครงการ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputdetail" class="col-sm-2 col-form-label"><b>รายระเอียด</b></label>
                                <div class="col-sm-10">
                                    <textarea  name="prj_detail" rows="4" cols="80" class="form-control" id="editinputdetail" placeholder="รายระเอียด"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputtype" class="col-sm-2 col-form-label"><b>ประเภท</b></label>
                                <div class="col-sm-10">
                                    <select id="inputtype" class="form-control" name="prj_prt_id">
                                        <option selected id="editinputtype" value=""></option>
                                        @foreach ($projecttype as $editprojecttypes)
                                            <option value="{{ $editprojecttypes->prt_id }}">{{ $editprojecttypes->prt_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputprovince" class="col-sm-2 col-form-label"><b>จังหวัด</b></label>
                                <div class="col-sm-10">
                                    <select id="inputprovince" class="form-control" name="prj_prn_id">
                                        <option selected id="editinputprovince"></option>
                                        @foreach ($province as $editprovinces)
                                            <option value="{{ $editprovinces->prn_id }}">{{ $editprovinces->prn_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputdatestart" class="col-sm-2 col-form-label"><b>เริ่ม</b></label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="prj_start_date" id="editinputdatestart">
                                </div>
                                <label for="inputdateend" class="col-sm-2 col-form-label"><b>สิ้นสุด</b></label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="prj_end_date" id="editinputdateend">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputbudget1" class="col-sm-2 col-form-label"><b>งบประมาณ</b></label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="prj_budget" id="editinputbudget1" placeholder="งบประมาณ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputbudget3" class="col-sm-2 col-form-label"><b>มีงบสนับสนุนอีก</b></label>
                                <div class="col-sm-1">
                                    <input type="radio" id="edithavemore" name="prj_is_support" value="1">
                                    <label class="col-sm-2 col-form-label" for="havemore">มี</label><br>
                                </div>
                                <div class="col-sm-1">
                                    <input type="radio" id="editnomore" name="prj_is_support" value="0">
                                    <label class="col-sm-2 col-form-label" for="nomore">ไม่มี</label><br>
                                </div>
                            </div>
                            <div id="editshowmorebudget" class="form-group row">
                                <label for="inputbudget2" class="col-sm-2 col-form-label"><b>งบประมาณ</b></label>
                                <div class="col-sm-10">
                                    <input type="number" name="prj_budget_support" class="form-control" id="editinputbudget2" placeholder="งบประมาณ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputstyle" class="col-sm-2 col-form-label"><b>ลักษณะโครงการ</b></label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="prj_is_improvement_project" id="editinputstyle1" value="1">
                                    <label class="col-sm-5 col-form-label" for="inputstyle1">โครงการปรับปรุงงาน (Improvement project)</label><br>
                                    <input type="checkbox" name="prj_is_innovation_project" id="editinputstyle2" value="1">
                                    <label class="col-sm-5 col-form-label" for="inputstyle2">โครงการนวัตกรรม (Innovative project)</label><br>
                                    <input type="checkbox" name="prj_is_research_project" id="editinputstyle3" value="1">
                                    <label class="col-sm-7 col-form-label" for="inputstyle3">โครงการวิจัยและพัฒนา (research and development project)</label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="color:#FFFFFF;" type="submit" class="btn btn-success"><i class="fa fa-save"></i> บันทึก</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{ asset('js/app-style-switcher.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('js/custom.js') }}"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="{{ asset('plugins/bower_components/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('js/pages/dashboards/dashboard1.js') }}"></script>
    <!--Data table-->
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.js"></script>
    <!--Other Script-->
    <script type="text/javascript" src="{{ asset('js/general.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/crud.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
