$(document).ready(function(){

    $('#addprojectform').on('submit', function(e){
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/addproject",
            data: $('#addprojectform').serialize(),
            success: function(){
                Swal.fire({
                    icon: 'success',
                    title: 'บันทึกเรียบร้อย',
                    html: '<h4 style="color:green;">ถ้าไม่มีการเปลี่ยนแปลงโปรดรีเฟรชหน้าใหม่อีกครั้ง</small>',
                    showConfirmButton: false,
                    timer: 1800
                })
                window.setTimeout(function(){
                    location.reload();
                } ,1700);
            },error:function(){
                Swal.fire({
                    icon: 'error',
                    title: 'บันทึกข้อมูลไม่สำเร็จ',
                    html: '<small style="color:red;">ถ้าไม่มีการเปลี่ยนแปลงโปรดรีเฟรชหน้าใหม่อีกครั้ง</small>',
                    showConfirmButton: false,
                    timer: 1800
                })
            }
        });
    });
});

$(document).ready(function (){
    $('.editproject').on('click',function(){
        var prjid = $(this).data('prjid');
        var prjname = $(this).data('prjname');
        var prjdetail = $(this).data('prjdetail');
        var prtname = $(this).data('prtname');
        var prjbudget = $(this).data('prjbudget');
        var prjbudgetsupport = $(this).data('prjbudgetsupport');
        var prnname = $(this).data('prnname');
        var prjstartdate = $(this).data('prjstartdate');
        var prjendtdate = $(this).data('prjenddate');
        var prjisimprovementproject = $(this).data('prjisimprovementproject');
        var prjisinnovationproject = $(this).data('prjisinnovationproject');
        var prjisresearchproject = $(this).data('prjisresearchproject');
        var prjissupport = $(this).data('prjissupport');
        var prjprtid = $(this).data('prjprtid');
        var prjprnid = $(this).data('prjprnid');
        $('#prjhiddenid').val(prjid);
        $('#editinputproject').val(prjname);
        $('#editinputtype').text(prtname);
        $('#editinputtype').val(prjprtid);
        $('#editinputprovince').text(prnname);
        $('#editinputprovince').val(prjprnid);
        $('#editinputdatestart').val(prjstartdate);
        $('#editinputdateend').val(prjendtdate);
        $('#editinputdetail').val(prjdetail);
        $('#editinputbudget1').val(prjbudget);
        $('#editinputbudget2').val(prjbudgetsupport);
        if (prjissupport == '1') {
            $('#edithavemore').prop('checked',true);
            $('#editshowmorebudget').show();
            $('#editinputbudget2').val(prjbudgetsupport);
        }
        else {
            $('#editnomore').prop('checked',true);
        }
        if (prjisimprovementproject == '1') {
            $('#editinputstyle1').prop('checked',true);
        }
        else {
            $('#editinputstyle1').prop('checked',false);
        }
        if (prjisinnovationproject == '1') {
            $('#editinputstyle2').prop('checked',true);
        }
        else {
            $('#editinputstyle2').prop('checked',false);
        }
        if (prjisresearchproject == '1') {
            $('#editinputstyle3').prop('checked',true);
        }
        else {
            $('#editinputstyle3').prop('checked',false);
        }
    });
});

$('#editprojectform').on('submit', function(e){
    var prjid = $('#prjhiddenid').val();
    e.preventDefault();

    $.ajax({
        type: "PUT",
        url: "/editproject/"+prjid,
        data: $('#editprojectform').serialize(),
        success: function(){
            Swal.fire({
                icon: 'success',
                title: 'บันทึกเรียบร้อย',
                html: '<h4 style="color:green;">ถ้าไม่มีการเปลี่ยนแปลงโปรดรีเฟรชหน้าใหม่อีกครั้ง</small>',
                showConfirmButton: false,
                timer: 1800
            })
            window.setTimeout(function(){
                location.reload();
            } ,1700);
        },error:function(){
            Swal.fire({
                icon: 'error',
                title: 'บันทึกข้อมูลไม่สำเร็จ',
                html: '<small style="color:red;">ถ้าไม่มีการเปลี่ยนแปลงโปรดรีเฟรชหน้าใหม่อีกครั้ง</small>',
                showConfirmButton: false,
                timer: 1800
            })
        }
    });
});
